question ASI :

ATELIER 1

''''
CROSS ORIGIN (Cross Origin Resource Sharing) : c'est un mécanisme qui permet de partager ses ressources aux autres domaines. En d'autres mots, 
les ressources restreintes ne sont plus réservés à l'utilisateur ou le domaine qui les a partagés.
Par exemple une requête AJAX à une API sur un autre domaine que celui du js, sera bloquée sur le CORS n'est pas activé.
"if you're running a React SPA that makes calls to an API backend running on a different domain. Web fonts also rely on CORS to work"
- https://auth0.com/blog/cors-tutorial-a-guide-to-cross-origin-resource-sharing/
dangereux car moins de traçabilité et de contrôle possible sur les informations partagés
''''

''''
ReactJS affiche rapidement la modification de composant en utilisant le virtual DOM (Document Object Model) 

When a component in ReactJS is updated, ReactJS first creates a new virtual DOM tree by applying the updates to the component.
In addition to the virtual DOM, ReactJS also uses techniques such as component state management, lifecycle methods, and event handling to optimize performance and minimize unnecessary updates. 
By using these techniques, ReactJS is able to deliver a fast and efficient UI rendering experience.
''''

''''
Le principe derrière FLUX est une transaction de donnée unilatérale entre la vue et le store puis de retour sur la vue.
Ce qui permets d'avoir une application dans un état consistant.
''''

''''
Redux is a state management library for JavaScript applications, commonly used with React. 
It provides a predictable state container for managing the state of an application and enables the building of scalable and maintainable applications.
''''

''''
JMS : Jakarta Message API  : https://en.wikipedia.org/wiki/Jakarta_Messaging
Part of Jakarta EE formerly Java EE Java 2 EE (extension de Java SE)
ce n'est pas spécifique à springboot
''''

''''
Point-to-Point (P2P): Dans le mode P2P, les messages sont envoyés d'un client à un autre client en utilisant une file d'attente. 
Un seul client peut recevoir chaque message, et chaque message est conservé dans la file d'attente jusqu'à ce qu'il soit reçu par un client.

Publish/Subscribe: Dans le mode Publish/Subscribe, les messages sont envoyés à plusieurs clients qui se sont abonnés à un "topic". 
Les messages sont conservés dans le topic jusqu'à ce que tous les clients qui se sont abonnés aient reçu le message. 
Les clients peuvent s'abonner et se désabonner à un topic à tout moment.
''''

''''
activemq : un message broker open source développé par Apache. Ils servent de passerelle. Ils reçoivent un message et le place en attente pour 
qu'un autre programme puisse le récupérer.
''''

''''
HTTP Request vs ActiveMQ : les requêtes http ont l'avantage d'être universelles et facilement mise en place.
Le message broker permets de centraliser les messages. Ce qui permets au programme de ne pas avoir à connaitre les autres programmes environnant
et seulement se contenter de communiquer via le message broker.
''''

''''
Partage de module maven : Pour le partage de module maven, il faut les compiler et publier les artifact (coordonnés du pom parents). Il existe plusieurs 
façons de publier ses artifacts. En local, il suffit d'utiliser les fonctions clean et install de maven pour placer les artifacts dans le M2REPO local.
Ensuite du côté du module où l'on souhtaite accéder à notre librairie externe, on ajoute une balise dependancy avec les coordonnés POM de la librairie
ext.
Pour publier ses artifacts nous pouvons utiliser un artifactory (repo d'artifact en résumé) interne ou externe/public comme gitlab par exemple.
''''

''''
on utilise la syntaxe ```export```
''''

''''
pour permettre à springboot de convertir le message d'activemq en Objet java, il faut impérativement que l'objet souhaité ait une classe à son nom
et que la méthode toString() soit surchargé. A vérifié :/
''''



''''
@EnableJpaRepositories
-   JPA : Java Persistence API     
Enable JPA Repositories. Will scan the package of the annotated configuration class for Spring Data repositories by default

@EntityScan
-	Annotation à ne pas confondre avec @Entity (étant mise sur les classes entités manipulées) ! 
Cette annotation va permettre de scan le package et trouver les classes entités et préciser à spring quelles seront les entités manipulées.

@ComponentScan
-	si paramétré, permets de scanner spécifiquement le package ciblé, sinon scan le package courant et ses sous-package.
permets de scanner le package et trouver les beans qui seront instanciés par Spring
''''










