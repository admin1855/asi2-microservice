# Frontend part

This frontend use backend:

    https://asi2.aiveo.fr/api/

## Inspirations

A github project:

    https://github.com/JustFS/redux-basics/tree/58e36f04e389515c7e6724c83bb2afdcaaabc359

Fake Backend

    https://github.com/cornflourblue/node-jwt-authentication-api

## Authentication
### Localhost
You need to use json-server and run:

    json-server --w src/assets/db.json --port 3003

And look file in:
    
    src/assests/db.json

Is the DB for users and cards

And use the fake backend in localhost to

    git clone https://github.com/cornflourblue/node-jwt-authentication-api
    cd node-jwt-authentication-api
    npm install
    npm start

look the readme project, service run on port 4000 (so you need to change url)

### DEV
If server is available, just do nothing 😊

For login:
username:
    
    jeremy.laurent

password:

    Pa$$w0rd