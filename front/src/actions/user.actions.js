import axios from "axios";
import {getItem} from "../services/LocaleStorage";

export const GET_USER_DATA = "GET_USER_DATA";

export const getUsers = (boolean) => {
    return (dispatch) => {
        if (boolean) {
            const userId = getItem('userId')
            console.log(userId)
        } else {
            dispatch({ type: GET_USER_DATA, payload: {} });
        }
    };
};
