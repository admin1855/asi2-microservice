import React, {useEffect, useRef, useState} from 'react';
import Navigation from "../components/Navigation";
import Footer from "../components/Footer";
import {Button, CardGroup, Col, Container, Form, Row, Spinner} from "react-bootstrap";
import ChatRoom from "../components/ChatRoom";
import {getUsers} from "../actions/user.actions";
import {useDispatch, useSelector} from "react-redux";
import {isEmpty} from "../Utils/project_lib";
import CardSelected from "../components/Cards/CardSelected";

const Play = () => {
    const dispatch = useDispatch();
    const [currentCard, setCurrentCard] = useState(null)
    const user = useSelector((state) => state.userReducer);
    const mounted = useRef();

    // Rooms creation
    const { roomId } = "AllRooms"
    const nameOfChat = "AllRooms"
    const [roomName, setRoomName] = React.useState("");

    const onCardClick = async (card) => {
        console.log("Start onCardClick")
        setCurrentCard(card)
        console.log("End onCardClick")
    };

    useEffect( () => {
        if (!mounted.current) {
            console.log("FirstPageLoading")
            dispatch(getUsers(true))
            mounted.current = true;
        }
    });

    return (
        <div className={"play_page"}>
            <Navigation/>
            <Container fluid="md" style={{padding: '30px'}}>
                <Row>
                    <Col>
                        <ChatRoom/>
                    </Col>
                    <Col>
                        <CardGroup>
                            {!isEmpty(user) &&
                            user.map((card) =>
                                <CardSelected
                                    pageName={"PLAY WITH"}
                                    card={card}
                                    key={card.id}
                                    onCardClick={onCardClick}/>
                            )}
                        </CardGroup>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <h3>Find Room, No room ? Create it one !</h3>
                        <Form>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Control
                                    type="text"
                                    placeholder="Room"
                                    value={roomName}
                                    className="text-input-field"
                                />
                            </Form.Group>
                        </Form>
                        <Button
                            variant="secondary"
                            className="send-message-button">
                            Create Room
                        </Button>

                        <Spinner animation="border" />
                    </Col>
                    <Col>
                        <Button
                            variant="outline-dark"
                            className="send-message-button">
                            Start the Fight
                        </Button>
                    </Col>
                </Row>
            </Container>
            <Footer/>
        </div>
    );
};

export default Play;
