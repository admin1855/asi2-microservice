package fr.softsouls.asi2.service;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Service
public class CallApiService {
    //Faites d'abord une demande
    private final RestTemplate restTemplate;

    public CallApiService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public float getPriceByCardId(Integer cardId) {
        try {
            final String url = "http://localhost:8080/api/card/card_price/"+cardId;
            final URI uri = UriComponentsBuilder.fromHttpUrl(url).path("/").build().toUri();
            System.out.println(uri);
            final RestOperations restTemplate = new RestTemplate();
            final ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);
            System.out.println(result.getBody());
            return Float.parseFloat(result.getBody());
        } catch (HttpStatusCodeException ex) {
            // raw http status code e.g `404`
            System.out.println(ex.getRawStatusCode());
            // http status code e.g. `404 NOT_FOUND`
            System.out.println(ex.getStatusCode().toString());
            // get response body
            System.out.println(ex.getResponseBodyAsString());
            // get http headers
            HttpHeaders headers= ex.getResponseHeaders();
            System.out.println(headers.get("Content-Type"));
            System.out.println(headers.get("Server"));
        }
        return 0;
    }

}
