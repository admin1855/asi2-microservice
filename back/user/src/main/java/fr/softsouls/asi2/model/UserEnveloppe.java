package fr.softsouls.asi2.model;

import java.io.Serializable;

public class UserEnveloppe implements Serializable {
    private static final long serialversionUID = 12345L;

    private String Action;
    private UserModel user;
    private Integer userId;

    public UserEnveloppe(String action, UserModel user) {
        Action = action;
        this.user = user;
    }

    public UserEnveloppe(String action, Integer userId) {
        Action = action;
        this.userId = userId;
    }

    public String getAction() {
        return Action;
    }

    public void setAction(String action) {
        Action = action;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }


    @Override
    public String toString() {
        return "UserEnveloppe{" +
                "Action='" + Action + '\'' +
                ", user=" + user.toString() +
                ", userId=" + userId +
                '}';
    }

}
