package fr.softsouls.asi2.controller;

import fr.softsouls.asi2.model.UserModel;
import fr.softsouls.asi2.service.BusService;
import fr.softsouls.asi2.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class UserRestController {

    @Autowired
    private UserService userService;

    @Autowired
    private BusService busService;

    @RequestMapping("/users")
    private List<UserModel> getAllUsers() {
        return userService.getAllUsers();
    }

    @RequestMapping("/user/{id}")
    private UserModel getUser(@PathVariable String id) {
        Optional<UserModel> ruser;
        ruser= userService.getUser(id);
        if(ruser.isPresent()) {
            return ruser.get();
        }
        return null;
    }

    @RequestMapping(method=RequestMethod.POST,value="/user")
    public void addUser(@RequestBody UserModel user) {
        busService.addUser(user);
    }

    @RequestMapping(method=RequestMethod.PUT,value="/user/{id}")
    public void updateUser(@RequestBody UserModel user,@PathVariable String id) {
        user.setId(Integer.valueOf(id));
        busService.updateUser(user);
    }

    @RequestMapping(method=RequestMethod.DELETE,value="/user/{id}")
    public void deleteUser(@PathVariable Integer id) {
        busService.deleteUser(id);
    }

    @RequestMapping(method=RequestMethod.GET,value="/auth")
    private int getAllCourses(@RequestParam("login") String login, @RequestParam("pwd") String pwd) {
        Optional<UserModel> user = userService.getUserByLoginPwd(login,pwd).stream().findFirst();
        if(user.isPresent()) {
            return user.get().getId();
        }
        return 0;
    }

    @RequestMapping("/account/{id}")
    private float getAccountByUserId(@PathVariable String id) {
        return userService.getAccountByUserId(Integer.valueOf(id));
    }

}
