package fr.softsouls.asi2.service;

import fr.softsouls.asi2.model.UserEnveloppe;
import fr.softsouls.asi2.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class BusListener {

    @Autowired
    JmsTemplate jmsTemplate;

    @Autowired
    private UserService userService;

    @JmsListener(destination = "BUSUSER_private", containerFactory = "connectionFactory")
    public void recieveAddUser(UserEnveloppe enveloppe) {
        System.out.println(enveloppe);
        if (enveloppe.getAction().equals("addUser")) {
            UserModel userModel = enveloppe.getUser();
            System.out.println("[BUSUSER_private] RECEIVED return from add user String MSG=["+userModel+"]");
            userService.addUser(userModel);
        }
    }

    @JmsListener(destination = "BUSUSER_private", containerFactory = "connectionFactory")
    public void recieveUpdateUser(UserEnveloppe enveloppe) {
        if (enveloppe.getAction().equals("updateUser")) {
            UserModel userModel = enveloppe.getUser();
            System.out.println("[BUSUSER_private] RECEIVED return from update user String MSG=["+userModel+"]");
            userService.updateUser(userModel);
        }
    }

    @JmsListener(destination = "BUSUSER_private", containerFactory = "connectionFactory")
    public void receiveDeleteUser(UserEnveloppe enveloppe) {
        if (enveloppe.getAction().equals("deleteUser")) {
            Integer idUser = enveloppe.getUserId();
            System.out.println("[BUSUSER_private] RECEIVED return from delete user String MSG=["+idUser+"]");
            userService.deleteUser(idUser);
        }
    }
}
