package fr.softsouls.asi2.repository;


import fr.softsouls.asi2.model.UserModel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<UserModel, Integer> {

    List<UserModel> findByLoginAndPwd(String login,String pwd);

}

