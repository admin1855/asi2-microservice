package fr.softsouls.asi2.service;

import fr.softsouls.asi2.model.UserModel;
import fr.softsouls.asi2.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;



@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BusService busService;

    RestTemplateBuilder builder=new RestTemplateBuilder();
    @Autowired
    CallApiService callApiService= new CallApiService(builder);

    public List<UserModel> getAllUsers() {
        List<UserModel> userList = new ArrayList<>();
        userRepository.findAll().forEach(userList::add);
        return userList;
    }

    public Optional<UserModel> getUser(String id) {
        return userRepository.findById(Integer.valueOf(id));
    }

    public Optional<UserModel> getUser(Integer id) {
        return userRepository.findById(id);
    }

    public void addUser(UserModel user) {
        //needed to avoid detached entity passed to persist error
        UserModel uBase=userRepository.save(user);
    }

    public void updateUser(UserModel user) {
        userRepository.save(user);

    }

    public void deleteUser(Integer id) {
        userRepository.deleteById(Integer.valueOf(id));
    }

    public List<UserModel> getUserByLoginPwd(String login, String pwd) {
        List<UserModel> ulist=null;
        ulist=userRepository.findByLoginAndPwd(login,pwd);
        return ulist;
    }


    public float getAccountByUserId(Integer id) {
        Optional<UserModel> c = this.userRepository.findById(id);
        if (c.isPresent()) {
            return c.get().getAccount();
        }
        return 0;
    }

    public void buyCard(Integer cardId, Integer userId) {
        Optional<UserModel> userToUpdate = this.getUser(userId);
        if (userToUpdate.isPresent()) {
            float cardPrice= callApiService.getPriceByCardId(cardId);
            userToUpdate.get().setAccount(userToUpdate.get().getAccount() - cardPrice);//le solde diminue
            userRepository.save(userToUpdate.get());
        }
    }

    public void sellCard(Integer cardId,Integer userId) {
        Optional<UserModel> userToUpdate = this.getUser(userId);
        if (userToUpdate.isPresent()) {
            float cardPrice= callApiService.getPriceByCardId(cardId);
            userToUpdate.get().setAccount(userToUpdate.get().getAccount() + cardPrice);// le solde augemente
            userRepository.save(userToUpdate.get());
        }
    }

}

