package fr.softsouls.asi2.service;

import fr.softsouls.asi2.model.UserEnveloppe;
import fr.softsouls.asi2.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;


@Service
public class BusService {

    @Autowired
    JmsTemplate jmsTemplate;

    public void addUser(UserModel userModel){
        UserEnveloppe enveloppe = new UserEnveloppe("addUser", userModel);
        System.out.println("[BUSUSER_private] SEND String MSG=[" + enveloppe + "]");
        jmsTemplate.convertAndSend("BUSUSER_private", enveloppe);
    }

    public void updateUser(UserModel userModel){
        UserEnveloppe enveloppe = new UserEnveloppe("updateUser", userModel);
        System.out.println("[BUSUSER_private] SEND String MSG=[" + enveloppe + "]");
        jmsTemplate.convertAndSend("BUSUSER_private", enveloppe);
    }

    public void deleteUser(Integer userid){
        UserEnveloppe enveloppe = new UserEnveloppe("deleteUser", userid);
        System.out.println("[BUSUSER_private] SEND String MSG=["+enveloppe+"]");
        jmsTemplate.convertAndSend("BUSUSER_private",enveloppe);
    }

}
