Avantage / Inconvénient Bus

####RabbitMQ####

#Pros of RabbitMQ Message Broker
It does not require a charge for transferring messages with highly available queue.
Most widely deployed open source message broker.
Developer easier experience with languages such as: Java, PHP, Python, Ruby.
Use Puppet, BOSH, Chef and Docker for deployment.
Cloud based applications: TLS, LDAP.
Management UI.
It is easily accessible as it is open source software.
Minimum data traffic.
Manageable with ability to add more queues and more services.
Effortless integration and configuration system.
Security, supports two authentication (2FA) ,SSL and several authorization backends.
It supports password based authentication.
It embraces Advanced Message Queueing Protocol (AMQP, HTTP, STOMP, MQTT).
It can scale one million messages in one second.


#Cons of RabbitMQ Message Broker
Not reliable for large data sets but Kafka is excellent in processing large datasets.
Non-transactional (by default).
Needs Erlang.
Premium integration services.
Issues with processing big amounts of data
The clustering has a confined amount of features and is quite complex while implementing RabbitMQ as clusters.
Lack of documentation.


####ActiveMQ####

#Pros of ActiveMQ Message Broker
It offers a variety of protocols such as; PHP, JAVA, C, C++.and, etc.
It contains modern features such as; message group, virtual queue and combined queue.
It allows users to use the browser as a message communication structure.
It can handle high level clusters and point to point communication.
It advocates Ajax which enables the ability of the browser to operate DHTML and utilize the browser as a message communication machine.
It provides web service projects through CXF and Axis.
It provides dual security through double layers of the SSL/TLS security layer.
The Rest API allows language autonomous web API.

#Cons of ActiveMQ Message Broker
Lack of assurance of the delivery of messages.
Messages must be sent to either queues or topics.
Inconvenient documentation.
Not much amiable to the non-apache users.